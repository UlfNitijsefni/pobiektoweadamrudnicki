package test.utils;

import main.model.FileOperations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileOperationsTest {

    private FileOperations fileOperations = new FileOperations();
    int[][] testArr = new int[][]{{2, 1, 2}, {1, 4, 1}, {2, 1, 2}};

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void saveStateToFileTest() {
        fileOperations.saveStateToFile(testArr, 3, 3, "output");
        assertTrue(true);
    }

    @Test
    void readFromFileTest() {
        int[][] testArr = new int[3][3];
        fileOperations.readFromFile(testArr, "output");
//        assertTrue(checkEquals(ModelBoard.boardValues, testArr));
        assertTrue(true);
    }


    private boolean checkEquals(int[][] expectedArr, int[][] actualArr){
        int height = expectedArr.length;
        int width = expectedArr[0].length;
        if(expectedArr.length != actualArr.length || expectedArr[0].length != actualArr[0].length){
            return false;
        }
        else{
            for(int y = 0; y < height; y++)
                for(int x = 0; x < width; x++){
                    if(expectedArr[x][y] != actualArr[x][y])
                        return false;
                }
            return true;
        }
    }
}