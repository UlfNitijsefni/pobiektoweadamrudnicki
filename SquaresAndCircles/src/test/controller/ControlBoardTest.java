package test.controller;

import main.controller.ControlBoard;
import main.controller.ControlHistory;
import main.model.FileOperations;
import main.model.ModelBoard;
import main.model.ModelBoardDefaults;
import main.model.history.Move;
import main.model.history.MoveHistory;
import main.utils.Utils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
class ControlBoardTest {

    @Mock
    FileOperations fileOperations;
    @Spy
    ModelBoardDefaults modelBoardDefaults;
    @Spy
    ModelBoard modelBoard;
    @Spy
    MoveHistory moveHistory;

    @InjectMocks
    ControlHistory controlHistory = new ControlHistory(moveHistory);
    Utils utils = Utils.getInstance();

    @InjectMocks
    private ControlBoard controlBoard = new ControlBoard(modelBoard, modelBoardDefaults, controlHistory, 6, 6, fileOperations);

    @BeforeEach
    void setUp() {
//        moveHistory = new MoveHistory(50);
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    // =================================================================================================================
    @Test
    void withHistoryEmpty_saveMove_savesOneMove() {
        controlBoard.saveMove(0, 0, 1);
        assertFalse(moveHistory.isEmpty());
    }

    @Test
    void withHistoryEmpty_undo_doesNothing() {
        Move move = controlBoard.undo();
        assertNull(move);
        assertTrue(moveHistory.isEmpty());
    }

    @Test
    void whenSaveMove_historyIndexEqualsSize() {
        controlBoard.saveMove(0, 0, 1);
        assertEquals(moveHistory.getIndex(), moveHistory.getSize() - 1);

        controlBoard.saveMove(1, 0, 2);
        controlBoard.saveMove(2, 0, 3);
        assertEquals(moveHistory.getIndex(), moveHistory.getSize() - 1);
    }

    @Test
    void withSaveMove_whenUndoRedo_returnsCorrect() {
        when(modelBoard.getFieldValue(0, 0)).thenReturn(0);
        when(modelBoard.getFieldValue(0, 2)).thenReturn(2);
        when(modelBoard.getFieldValue(0, 4)).thenReturn(4);

        Move actual, expected;

        controlBoard.saveMove(0, 0, 1);
        actual = controlBoard.undo();
        expected = new Move(0, 0, 0, 1);
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));

        actual = controlBoard.redo();
        expected = new Move(0, 0, 0, 1);
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));
    }

    @Test
    void withSaveMove_whenUndoRedoMulti_returnsCorrect() {
        when(modelBoard.getFieldValue(0, 0)).thenReturn(0);
        when(modelBoard.getFieldValue(2, 0)).thenReturn(2);
        when(modelBoard.getFieldValue(4, 0)).thenReturn(4);

        Move actual, expected;

        controlBoard.saveMove(0, 0, 1);
        controlBoard.saveMove(2, 0, 3);
        controlBoard.saveMove(4, 0, 5);

        actual = controlBoard.undo();
        expected = new Move(4, 0, 4, 5);
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));

        actual = controlBoard.redo();
        expected = new Move(4, 0, 4, 5);
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));
    }

    @Test
    void withSomeHistory_undo_getsPreviousMove() {
        controlBoard.saveMove(0, 0, 1);
        controlBoard.saveMove(0, 2, 2);
        controlBoard.saveMove(0, 4, 3);

        Move expected, actual;

        expected = new Move(0, 4, 0, 3);
        actual = controlBoard.undo();
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));

        expected = new Move(0, 2, 0, 2);
        actual = controlBoard.undo();
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));

        expected = new Move(0, 0, 0, 1);
        actual = controlBoard.undo();
        assertEquals(utils.moveToString(expected), utils.moveToString(actual));

        actual = controlBoard.undo();
        assertNull(actual);
    }

    @Test
    void whenSavedMultipleMoves_reset_SaveMove(){
        controlBoard.saveMove(2, 0, 1);
        controlBoard.saveMove(2, 1, 1);
        controlBoard.saveMove(2, 2, 1);

        controlHistory.clearHistory();

        controlBoard.saveMove(2, 0, 1);
    }
}