package test.controller;

import main.model.history.Move;
import main.model.history.MoveHistory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MoveHistoryTest {

    private MoveHistory moveHistory;

    @BeforeEach
    public void setUp() {
        moveHistory = new MoveHistory(20);
    }

    @Test
    void withEmptyHistory_isIndexFirstLast_returnsTRUE() {
        assertTrue(moveHistory.isIndexFirst());
        assertTrue(moveHistory.isIndexLast());
    }

    @Test
    void withSomeHistory_isIndexFirstLast_returnCorrect() {
        moveHistory.addMove(new Move(1, 0, 1, 2));
        moveHistory.incremIndex();
        moveHistory.addMove(new Move(2, 0, 2, 3));
        moveHistory.incremIndex();
        moveHistory.addMove(new Move(3, 0, 3, 4));
        moveHistory.incremIndex();
        assertTrue(moveHistory.isIndexLast());
        assertFalse(moveHistory.isIndexFirst());

        moveHistory.decremIndex();
        moveHistory.decremIndex();
        moveHistory.decremIndex();
        assertTrue(moveHistory.isIndexFirst());
    }

    @Test
    void withHistoryEmpty_isEmpty_returnsTRUE() {
        assertTrue(moveHistory.isEmpty());
        assertEquals(-1, moveHistory.getIndex());
    }


    @Test
    void withHistoryEmpty_addMove_addsOneMove() {
        moveHistory.addMove(new Move(0, 0, 0, 1));
        assertFalse(moveHistory.isEmpty());
        assertEquals(1, moveHistory.getSize());
        assertEquals(-1, moveHistory.getIndex());

        moveHistory.addMove(new Move(1, 0, 1, 2));
        moveHistory.addMove(new Move(2, 0, 2, 3));
        moveHistory.addMove(new Move(3, 0, 3, 4));
        assertEquals(4, moveHistory.getSize());
        assertEquals(-1, moveHistory.getIndex());
    }

    @Test
    void withSomeHistory_incremDecremIndex_changesIndexByOne() {
        assertEquals(-1, moveHistory.getIndex());

        moveHistory.incremIndex();
        moveHistory.incremIndex();
        assertEquals(1, moveHistory.getIndex());

        moveHistory.decremIndex();
        moveHistory.decremIndex();
        assertEquals(-1, moveHistory.getIndex());
    }

    @Test
    void withSomeHistory_dropTail_works() {
        // should not throw
        moveHistory.dropTail();

        moveHistory.addMove(new Move(0, 0, 0, 1));
        moveHistory.incremIndex();
        moveHistory.decremIndex();
        moveHistory.dropTail();
        assertTrue(moveHistory.isEmpty());
        assertEquals(0, moveHistory.getSize());

        moveHistory.addMove(new Move(1, 0, 1, 2));
        moveHistory.incremIndex();
        moveHistory.addMove(new Move(2, 0, 2, 3));
        moveHistory.incremIndex();
        moveHistory.addMove(new Move(3, 0, 3, 4));
        moveHistory.incremIndex();

        moveHistory.decremIndex();
        moveHistory.decremIndex();
        moveHistory.dropTail();
        assertEquals(1, moveHistory.getSize());
    }
}