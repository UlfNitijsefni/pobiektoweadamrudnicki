//package test.model;
//
//import main.application.Main;
//import main.utils.Utils;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class ModelBoardTest {
//
//    private Main main = Main.getInstance();
//
//    @BeforeEach
//    void setUp() {
////        main.appGraph.boardModel.resetToDefault();
//    }
//
//    @AfterEach
//    void tearDown() {
//    }
//
//    @Test
//    void setFieldValue() {
//        int expected = 1;
//        modelBoard.setFieldValue(1, 0, 0);
//        int actual = modelBoard.getFieldValue(0, 0);
//
//        assertEquals(expected, actual);
//    }
//
//    @Test
//    void getFieldValue() {
//        int expected = 0;
//        int actual = modelBoard.getFieldValue(0, 0);
//
//        assertEquals(expected, actual);
//    }
//
//
//    @Test
//    void getAll() {
//        int[][] expectedArr = new int[6][6];
//        expectedArr[2][2] = 2;
//        expectedArr[3][3] = 3;
//        expectedArr[4][4] = 4;
//
//        modelBoard.setFieldValue(2, 2, 2);
//        modelBoard.setFieldValue(3, 3, 3);
//        modelBoard.setFieldValue(4, 4, 4);
//        int[][] actualArr = modelBoard.getBoardValues();
//
//        String expected = Utils.intArrayToString(6, 6, expectedArr);
//        String actual = Utils.intArrayToString(6, 6, actualArr);
//
//        assertEquals(expected, actual);
//    }
//}