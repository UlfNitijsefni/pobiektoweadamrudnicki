package main.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class IntStringChanger {

    //---------------------------------------------------------------------------------
    public static String intToString(int[][] toChange, int height, int width) {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < height; i++) {
            if (i > 0) stringBuilder.append("\n");
            for (int j = 0; j < width; j++) {
                if (j > 0) stringBuilder.append(" ");
//                stringBuilder.append(String.valueOf(toChange[i][j]));
                stringBuilder.append(toChange[i][j]);
            }
        }
        return String.valueOf(stringBuilder);
    }

    //---------------------------------------------------------------------------------
    public static int[][] stringToArrInt(String fileName) {

        int height = 0;
        int width = 0;

        //Read height and width from beginning of the file
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            {
                height = Integer.parseInt(br.readLine());
                width = Integer.parseInt(br.readLine());
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }

        //TODO Check if coordinates are in correct order
        int[][] convertedString = new int[width][height];
        //---------------------------------------
        try (
                BufferedReader br = new BufferedReader(new FileReader(fileName))) {

            //This loop allows us to skip straight to the 3rd line, in which our array begins.
            for (int l = 0; l < 2; l++) {
                br.readLine();
//                    System.out.println(br.readLine());
            }
            for (int y = 0; y < height; y++) {

                //Fills array convertedString with values from the saved array, row after row
                for (String line; (line = br.readLine()) != null; ) {
                    String[] arrOfStr = line.split(" ");
                    for (int i = 0; i < arrOfStr.length; i++) {
                        convertedString[i][y] = Integer.parseInt(arrOfStr[i]);
                    }
                }
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return convertedString;
    }
}

