package main.utils;

import main.model.history.Move;

public class Utils {

    private static Utils instance = new Utils();
    public static Utils getInstance() {return instance;}

    public String intArrayToString(int height, int width, int[][] srcArray){

        StringBuilder stringBuilder = new StringBuilder();

        try {
            for(int i = 0; i < height; i++){
                for(int j = 0; j < width; j++){
                    stringBuilder.append(Integer.toString(srcArray[i][j]));
                    stringBuilder.append(",");
                }
                stringBuilder.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * Checks whether the element provided is a square or a circle; true = square, false = circlse
     *
     * @param i x of boardField
     * @param j y of boardField
     * @return whether the element is a square or not (in case it's not, it's a circle)
     */
    public boolean isSquare(int i, int j) {
        return (i + j) % 2 == 0;
    }


    //------------------------------------------------------------------------------
    public boolean isMoveEqual(Move a, Move b) {
        return a.x == b.x && a.y == b.y && a.oldValue == b.oldValue && a.newValue ==b.newValue;
    }

    public String moveToString(Move move){
        return move.x + "-" + move.y + "-" + move.oldValue + "-" + move.newValue;
    }
}