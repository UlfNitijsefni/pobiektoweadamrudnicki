package main.view;

import javafx.scene.control.Button;
import main.view.boardField.BoardField;

public class MyButton extends Button implements Coloring {

    private MyButton() {
    }

    //------------------------------------------------------------------------------
    public static class Builder{

//        return this;
        private int height;
        private int width;
        private String text;

        public Builder(String text) {
            this.text = text;
        }

        public Builder withHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder withWidth(int width) {
            this.width = width;
            return this;
        }

        //------------------------------------------------------------------------------
        public MyButton build() {
            MyButton boardField = new MyButton();
            boardField.setMinSize(this.width, this.height);
            boardField.setMaxSize(this.width, this.height);
            boardField.setPrefSize(this.width, this.height);
            boardField.setText(this.text);
//            boardField.setPrefSize(this.height, this.width);

            return boardField;
        }
    }

    @Override
    public void setColor(boolean correct) {
        String color = "green";
        if (correct)
            this.setStyle("-fx-border-color: " + color + "; -fx-border-width: 5");
    }
}