package main.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import main.controller.ControlBoard;
import main.controller.ValueType;
import main.model.history.Move;
import main.model.history.MoveHistory;
import main.utils.Utils;
import main.view.boardField.BoardField;
import main.view.boardField.Circle;
import main.view.boardField.ExampleRectangle;
import main.view.boardField.Square;

import java.util.ArrayList;

import static main.controller.ValueType.NEW_VALUE;
import static main.controller.ValueType.OLD_VALUE;

public class GameView {

    private ControlBoard controlBoard;
    private MoveHistory moveHistory;

    private GridPane gameSetGrid;

    private int paddingSize = 10;
    private BoardField[][] textFieldArr;

    private int windowWidth, windowHeight;
    private int width, height;
    private Scene scene;

    // CONSTRUCTOR
    // =========================================================================================================
    public GameView(ControlBoard controlBoard, MoveHistory moveHistory,
                    int windowWidth, int windowHeight, int width, int height) {
        this.controlBoard = controlBoard;
        this.moveHistory = moveHistory;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this.width = width;
        this.height = height;

        textFieldArr = new BoardField[width][height];
        createBoard();
        fillBoardAtStart(controlBoard.getDefaultValues());
    }

    public Scene getScene() {
        return scene;
    }

    // API
    // =========================================================================================================


    /**
     * Fills board with provided values at the beginning
     *
     * @param values values to use
     */
    public void fillBoardAtStart(int[][] values) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (values[i][j] != 0) {
                    setFieldValue(i, j, values[i][j]);
                    textFieldArr[i][j].setEditable(false);
                }
            }
        }
    }

    /**
     * Fills board with provided values
     *
     * @param values values to use
     */
    public void fillBoard(int[][] values) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (values[i][j] != 0)

                    setFieldValue(i, j, values[i][j]);
            }
        }
    }

    //------------------------------------------------------------------------

    /**
     * Changes all fields backgrounds to green assuming all are correct and then changes all incorrect, taken from
     * incorrectFields, to red
     *
     * @param incorrectFields list of incorrect fields coordinates
     */

    private void showResult(ArrayList<int[]> incorrectFields) {
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                if (Utils.getInstance().isSquare(x, y))
                    setFieldOnCorrect(x, y, true, gameSetGrid);

        for (int i = 0; i < incorrectFields.size(); i++) {
            setFieldOnCorrect(incorrectFields.get(i)[0], incorrectFields.get(i)[1], false, gameSetGrid);
        }
    }

    //------------------------------------------------------------------------

    /**
     * Displays values from provided array on the game board in corresponding board fields
     */

    public void fillGameBoardFromArr() {
        int[][] numbersFromBoard = controlBoard.getCurrGameState();
        int[][] baseValues = controlBoard.getDefaultValues();
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
                setFieldValue(x, y, numbersFromBoard[x][y]);
                if (baseValues[x][y] != 0) {
                    textFieldArr[x][y].setEditable(false);
                    setFieldValue(x, y, baseValues[x][y]);
                }
            }
    }

    //------------------------------------------------------------------------

    /**
     * Changes color of field background depending whether it's correct or not.
     *
     * @param x           X of the field
     * @param y           Y of the field
     * @param correct     Whether the field is correct
     * @param gameSetGrid Grid in which the field is
     */
    public void setFieldOnCorrect(int x, int y, boolean correct, GridPane gameSetGrid) {
        if (correct)
            getNodeFromGridPane(x, y, gameSetGrid).setStyle("-fx-background-color: rgba(0, 255, 0);");
        else
            getNodeFromGridPane(x, y, gameSetGrid).setStyle("-fx-background-color: rgba(255, 0, 0);");
    }

    // INIT
    // =========================================================================================================
    public void createBoard() {
        gameSetGrid = new GridPane();

        gameSetGrid.setMinSize(windowWidth, windowHeight);
        gameSetGrid.setPadding(new Insets(paddingSize, paddingSize, paddingSize, paddingSize));

        createBoardFields(gameSetGrid, width, height);

        //Creates button VBox and fills it with content
        VBox buttonBox = populateButtonBox(gameSetGrid);

        gameSetGrid.setStyle(" -fx-border-color: blue;  -fx-border-width: 2 ; ");

        HBox mainHBox = new HBox(gameSetGrid, buttonBox);
        buttonBox.setPrefSize(windowWidth / 4.0, windowHeight);
        buttonBox.setMinSize(windowWidth / 4.0, windowHeight);

        //------------------------------------------------------------------------
        //Creating a scene object
        scene = new Scene(mainHBox);
    }

    //------------------------------------------------------------------------

    /**
     * Initializes and sets up all buttons, including their event handlers and styles
     *
     * @param gameSetGrid Required for some event listeners
     * @return VBox with buttons
     */
    private VBox populateButtonBox(GridPane gameSetGrid) {
        VBox legendHbox = new VBox();
        MyButton saveButton = new MyButton.Builder("SAVE GAME").withHeight(40).withWidth(windowWidth / 8).build();
        MyButton checkButton = new MyButton.Builder("CHECK").withHeight(40).withWidth(windowWidth / 8).build();
        MyButton resetButton = new MyButton.Builder("RESET").withHeight(40).withWidth(windowWidth / 8).build();
        MyButton retreatButton = new MyButton.Builder("UNDO").withHeight(40).withWidth(windowWidth / 8).build();
        MyButton forwardButton = new MyButton.Builder("NEXT MOVE").withHeight(40).withWidth(windowWidth / 8).build();
//        MyButton testButton = new MyButton.Builder("DEFAULT").withHeight(40).withWidth(windowWidth / 8).build();
        MyButton loadButton = new MyButton.Builder("LOAD FROM FILE").withHeight(40).withWidth(windowWidth / 6).build();

        resetButton.setColor(true);

//        testButton.setAlignment(Pos.CENTER);

        createLegend(legendHbox);
        VBox buttonBox = new VBox(saveButton,
                checkButton,
                legendHbox,
                resetButton,
                addInstructions(),
                retreatButton,
                forwardButton,
//                testButton,
                loadButton);

        Insets insets = new Insets(10, 10, 10, 10);

        buttonBox.setSpacing(25);
        buttonBox.setPadding(insets);
        buttonBox.setAlignment(Pos.TOP_CENTER);

        setButtonActions(gameSetGrid, saveButton, checkButton, resetButton, retreatButton, forwardButton, loadButton);
//        setButtonActions(gameSetGrid, saveButton, checkButton, resetButton, retreatButton, forwardButton, testButton, loadButton);

        return buttonBox;
    }

    /**
     * Assigns each button corresponding action on being clicked, action explained in button names.
     *
     * @param gameSetGrid Required for few button actions
     *                    Parameters not explained; their names speak for themselves
     */
    private void setButtonActions(GridPane gameSetGrid,
                                  MyButton saveButton,
                                  MyButton checkButton,
                                  MyButton resetButton,
                                  MyButton retreatButton,
                                  MyButton forwardButton,
//                                  Button testButton,
                                  MyButton loadButton) {

        //Currently testButton doesn't do anything other than show a test dialog window
//        testButton.setOnAction(e -> {
//            // testTextInputDialog.setHeaderText("enter your name");
//            TextInputDialog testTextInputDialog = new TextInputDialog("enter any text");
//            testTextInputDialog.show();
//        });

        //

        //Adds event listener to resetButton so when it's clicked the game state & history are reset
        resetButton.setOnAction(value -> {
            fillBoard(controlBoard.getDefaultValues());
            fillGameBoardFromArr();
            resetHistory();
            resetColorSquares();
        });

        //Adds event listener to checkButton so when it's clicked it checks whether current game state is a winnig one
        checkButton.setOnAction(value -> {
            ArrayList<int[]> incorrectFields = controlBoard.isVictorious(gameSetGrid);
            showResult(incorrectFields);
        });

        // UNDO, REDO
        // -----------------------------------------------------------
        //These methods speak for themselves
        retreatButton.setOnAction(value -> {
            undoMove();
        });
        forwardButton.setOnAction(value -> {
            redoMove();
        });

        //Read/Save from/to file
        // -----------------------------------------------------------
        saveButton.setOnAction(value -> {
            saveButton.setColor(true);
            controlBoard.saveGame();
        });

        loadButton.setOnAction(value -> {
            saveButton.setColor(true);
            int[][] fromFile = controlBoard.loadSave();
            fillBoard(fromFile);
        });
    }

    /**
     * Retrieves last move and then using it sets correct field to it's former state. Second if makes sure that, in case
     * of some error in code, uneditable fields are not changed
     */
    private void undoMove() {
        Move lastMove = controlBoard.undo();
        if (lastMove == null) return;
        if (textFieldArr[lastMove.x][lastMove.y].isEditable())
            setFieldValue(lastMove, OLD_VALUE);
    }

    /**
     * Retrieves last move and then using it sets correct field to it's next state. Second if makes sure that, in case
     * of some error in code, uneditable fields are not changed
     */
    private void redoMove() {
        Move nextMove = controlBoard.redo();
        if (nextMove == null) return;
        setFieldValue(nextMove, NEW_VALUE);
    }

    //------------------------------------------------------------------------

    /**
     * Creates colorful squares and their descriptions so the user knows what color indicates what
     *
     * @param legendHBox HBox the legend is inserted into
     */
    private void createLegend(VBox legendHBox) {

        ExampleRectangle redRec = new ExampleRectangle(Color.RED);
        ExampleRectangle blueRec = new ExampleRectangle(Color.BLUE);
        ExampleRectangle greenRec = new ExampleRectangle(Color.GREEN);
        ExampleRectangle blackRec = new ExampleRectangle(Color.BLACK);

        Text correctText = new Text("  Poprawny");
        Text incorrectText = new Text("  Niepoprawny");
        Text squareText = new Text("  Kwadrat");
        Text circleText = new Text("  Kolo");

        HBox definitionCorrect = new HBox(greenRec, correctText);
        HBox definitionIncorrect = new HBox(redRec, incorrectText);
        HBox definitionSquare = new HBox(blueRec, squareText);
        HBox definitionTrue = new HBox(blackRec, circleText);

        legendHBox.getChildren().addAll(definitionCorrect, definitionIncorrect, definitionSquare, definitionTrue);
        legendHBox.setSpacing(15);
    }

    //------------------------------------------------------------------------

    /**
     * Fill game grid with textfields, with the text inputed into them aligned in the middle of the field
     *
     * @param gameSetGrid Grid we're inserting fields into
     * @param columns     Effectively width of the field
     * @param rows        Effectively height of the field
     */
    private void createBoardFields(GridPane gameSetGrid, int columns, int rows) {
        int fieldWidth = (windowWidth - 2 * paddingSize) / height;
        int fieldHeight = (windowHeight - 2 * paddingSize) / width;

        BoardField boardField;

        for (int x = 0; x < columns; x++) {
            for (int y = 0; y < rows; y++) {
                if (Utils.getInstance().isSquare(x, y)) {
                    boardField = new Square(controlBoard, fieldWidth, fieldHeight, x, y);
                } else
                    boardField = new Circle(controlBoard, fieldWidth, fieldHeight, x, y);

                boardField.addBorderOfColor("navy", 5);

                addField(boardField, gameSetGrid, x, y);
            }
        }
    }

    //------------------------------------------------------------------------
    private void addField(BoardField boardField, GridPane gameSetGrid, int x, int y) {
        gameSetGrid.add(boardField, x, y);
        this.textFieldArr[x][y] = boardField;
    }

    //------------------------------------------------------------------------

    /**
     * Sets field value when provided with its x and y coordinates
     *
     * @param x          X of field we're changing
     * @param y          Y of field we're changing
     * @param valueToSet Value we're putting into the field
     */
    public void setFieldValue(int x, int y, int valueToSet) {
        textFieldArr[x][y].setText("" + valueToSet);
    }

    //------------------------------------------------------------------------

    /**
     * Translates Move type object into values the <code>setFieldValue</code> using x, y and value can use
     *
     * @param move      Move to be translated
     * @param valueType Whether it's a new or and old value
     */
    public void setFieldValue(Move move, ValueType valueType) {
        int value = valueType == NEW_VALUE ? move.newValue : move.oldValue;
        setFieldValue(move.x, move.y, value);
    }

    //------------------------------------------------------------------------

    /**
     * Retrieves a node from gridpane so other methods can work on it.
     *
     * @param col      column we're looking in
     * @param row      row we're looking in
     * @param gridPane Gridpane we're looking in
     * @return Returns a node of Gridpane (In out program - BoardField)
     */
    private Node getNodeFromGridPane(int col, int row, GridPane gridPane) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    //------------------------------------------------------------------------

    /**
     * Similarly to <code>createLegend</code>, adds instructions about how to play the game
     *
     * @return Returns label that's later incorporated into view
     */
    private Label addInstructions() {
        Label instructions = new Label("Insert values ranging from 1 to 10 into fields in such a manner that" +
                " values in adjacement circles sum up to the value in the square they surround.");
        instructions.setWrapText(true);
//        addInstructions().setAlignment(Pos.CENTER);
        return instructions;
    }

    //------------------------------------------------------------------------

    /**
     * Sets all squares to their original color, usually after the checking method
     */
    private void resetColorSquares() {
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
                if (Utils.getInstance().isSquare(x, y))
                    getNodeFromGridPane(x, y, gameSetGrid).setStyle("-fx-background-color: rgba(176,224,230);");
            }
    }

    //------------------------------------------------------------------------

    /**
     * calls controlBoards' history cleansing method
     */
    private void resetHistory() {
        controlBoard.resetHistory();
    }
}
