package main.view.boardField;

import main.controller.ControlBoard;

public class Circle extends BoardField{


    public Circle(ControlBoard controlBoard, int width, int height, int x, int y) {
        super(controlBoard, width, height, x, y);
    }

    @Override
    /**
     * Sets circles to black for ease of distinction
     */
    public void draw() {
        //TODO Set color to one associated with circle
        this.setStyle("-fx-control-inner-background: black");
    }

}
