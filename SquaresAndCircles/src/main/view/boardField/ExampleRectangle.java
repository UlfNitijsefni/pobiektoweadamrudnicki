package main.view.boardField;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Rectangle that will be displayed in the legend, set to color provided in parameters
 */
public class ExampleRectangle extends Rectangle {
    public ExampleRectangle(Color toPaint){
        this.setFill(toPaint);
        this.setHeight(20);
        this.setWidth(20);
    }
}
