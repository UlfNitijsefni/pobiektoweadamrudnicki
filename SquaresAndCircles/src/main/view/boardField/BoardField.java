package main.view.boardField;


import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import main.controller.ControlBoard;
import main.model.FileOperations;
import main.utils.Utils;

import java.awt.event.FocusEvent;

/**
 * The most basic part of the game board; singular textfield that also holds it's coordinates and can reach to
 * controlBoard to do few operations
 */
public abstract class BoardField extends TextField {

    private int X;
    private int Y;
    private ControlBoard controlBoard;

    //ABSTRACT
    //==========================================
    public abstract void draw();

    //==========================================
    public BoardField(ControlBoard controlBoard, int width, int height, int x, int y) {
        this.controlBoard = controlBoard;
        this.setPrefSize(width, height);
        this.setAlignment(Pos.CENTER);
        this.X = x;
        this.Y = y;
        this.setFont(Font.font(width / 3.0));
        this.draw();
        addEnterListener();
        addChangeListener();
//        addFocusListener();
    }

    //------------------------------------------------------------------------------

    /**
     * Whenever value is entered and ENTER key is pressed, it's saved
     */
    private void addEnterListener() {
        this.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode().equals(KeyCode.ENTER)){
                    storeNextMove();
                }
            }
        });
    }

//    //------------------------------------------------------------------------------
//    private void addFocusListener() {
//        this.focusedProperty().addListener((ov, oldV, newV) -> {
//            storeNextMove();
//        });
//    }

    //------------------------------------------------------------------------------
    private void storeNextMove() {
        String temp = getText();
        if (!temp.isEmpty())
            controlBoard.saveMove(X, Y, Integer.parseInt(getText()));
//            controlBoard.storeFieldValue(X, Y, Integer.parseInt(getText()));
    }

    //------------------------------------------------------------------------------

    /**
     * In conjunction with <code>isInScope</code> method checks whether the inserted value is in between 1 and 10, in
     * case it's not it's automatically chaged to last correct state; Removes last char, effectively making inserting
     * out of scope values impossible.
     */
    private void addChangeListener() {
        this.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!isInScope(newValue)) {
                this.setText(this.getText().substring(0, this.getText().length() - 1));
            }
        });
    }

    //------------------------------------------------------------------------------

    /**
     * Checks whether newValue is in between 1 and 10
     *
     * @param newValue Value to check
     * @return Answer to the question
     */
    private boolean isInScope(String newValue) {
        if (newValue.length() == 0)
            return true;

        int lastCharASCII = newValue.charAt(newValue.length() - 1);

        if (lastCharASCII >= '0' && lastCharASCII <= '9') {
            int toCheck = Integer.parseInt(newValue);
            return (toCheck >= 1 && toCheck <= 10);
        } else {
            return false;
        }
    }

    //------------------------------------------------------------------------------

    /**
     * Adds border to the boardField with width and color provided
     *
     * @param color Color we want to set the border to
     * @param width Width of the border
     */
    public void addBorderOfColor(String color, int width) {
        setBorder(new Border(new BorderStroke(Paint.valueOf(color), BorderStrokeStyle.SOLID, null, new BorderWidths(width))));
    }

    //------------------------------------------------------------------------------
    public int getX() {
        return this.X;
    }

    //------------------------------------------------------------------------------
    public int getY() {
        return this.Y;
    }

    //------------------------------------------------------------------------------
    public void setX(int toSet) {
        this.X = toSet;
    }

    //------------------------------------------------------------------------------
    public void setY(int toSet) {
        this.Y = toSet;
    }

    //------------------------------------------------------------------------------
    public void setValue(int value) {
        this.setText("" + value);
    }
}
