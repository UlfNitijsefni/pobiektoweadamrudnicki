package main.view.boardField;

import main.controller.ControlBoard;
import main.view.Coloring;

public class Square extends BoardField implements Coloring {

    public Square(ControlBoard controlBoard, int width, int height, int x, int y) {
        super(controlBoard, width, height, x, y);

        //TODO: Text is barely visible as grey on dark blue. Change to white to match circles
    }

    /**
     * Sets background color of text in the rectangle to powderblue for ease of reading and distinction
     */
    @Override
    public void draw() {
        this.setStyle("-fx-control-inner-background: powderblue");

    }

    /**
     * Used to set the fields' color depending on whether it's correct or not
     */
    @Override
    public void setColor(boolean correct) {
        if (correct) {
//            this.setStyle("-fx-control-inner-background: green");
            String color = "green";
            addBorderOfColor(color, 5);
            //TODO Set it to green
        } else {
            String color = "red";
            addBorderOfColor(color, 5);
//            this.setStyle("-fx-control-inner-background: red");
            //TODO Set it to red
        }
    }


    public void lockIt() {
        this.setEditable(false);
    }
}