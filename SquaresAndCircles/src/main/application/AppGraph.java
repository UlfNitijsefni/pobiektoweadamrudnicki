package main.application;

import main.controller.ControlBoard;
import main.controller.ControlHistory;
import main.model.history.MoveHistory;
import main.model.FileOperations;
import main.model.ModelBoard;
import main.model.ModelBoardDefaults;
import main.view.GameView;

public class AppGraph {

    // game config
    private Config config = new Config();
    private int windowWidth = config.windowWidth;
    private int windowHeight = config.windowHeight;
    private int width = config.width;
    private int height = config.height;
    private int historyLength = config.historyLength;

    // model
    private ModelBoard modelBoard = new ModelBoard(width, height);
    private MoveHistory moveHistory = new MoveHistory(historyLength);
    private ModelBoardDefaults modelBoardDefaults = new ModelBoardDefaults();
    private FileOperations fileOperations = new FileOperations();

    // controller
    private ControlHistory controlHistory = new ControlHistory(moveHistory);
    private ControlBoard controlBoard = new ControlBoard(
            modelBoard,
            modelBoardDefaults,
            controlHistory,
            width, height,
            fileOperations);

    // view
    private GameView gameView = new GameView(controlBoard, moveHistory, windowWidth, windowHeight, width, height);

    // getters
    public GameView getGameView() {
        return gameView;
    }
}
