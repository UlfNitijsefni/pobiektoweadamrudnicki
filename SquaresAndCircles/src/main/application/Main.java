//package java.java.view;
package main.application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    // =================================================================================================================
    //In this scene we decide if the user wants to play default game or configure his own version of board
    @Override
    public void start(Stage stage) {
        populateStage(stage);
        stage.setTitle("Squares and circles: the game");
//        //Make app open in fullscreen mode.
//        stage.setFullScreen(true);
        stage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }

    private void populateStage(Stage stage) {
        // create all objects with their dependencies in the application graph
        AppGraph appGraph = new AppGraph();
        Scene scene = appGraph.getGameView().getScene();
        stage.setScene(scene);
    }
}