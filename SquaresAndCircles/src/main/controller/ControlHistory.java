package main.controller;

import main.model.history.LastDirection;
import main.model.history.Move;
import main.model.history.MoveHistory;

import static main.model.history.LastDirection.BACK;
import static main.model.history.LastDirection.FORWARD;

/**
 * Operates on current game move history
 */
public class ControlHistory {

    MoveHistory moveHistory;

    // For Mockito
    public ControlHistory() {}


    public ControlHistory(MoveHistory moveHistory) {
        this.moveHistory = moveHistory;
    }

    /**
     * Adds the move provided to the end of moveList, and takes care of maximum move range, removing oldest moves as the
     * limit is reached.
     *
     * @param move Move we add to the list of moves made to date
     */

    public void save(Move move) {
        if (!moveHistory.isIndexLast()) {
            moveHistory.dropTail();
            moveHistory.setIndex(moveHistory.getSize() - 1);
        }

        moveHistory.incremIndex();
        moveHistory.addMove(move);
        moveHistory.setLastDirection(FORWARD);
    }

    /**
     *
     * @return returns object Move to use in other methods
     * The move  we need is obtained depending on which direction we went last time and whether there are moves left
     * to go in the direction we want. lastDirection shows us whether we went forward or backward in move history. If we
     * went forward and want to go forward, it works normally. Backward + backward = normally. Backward and then forward
     * or the other way around - we only switch values from last move and NOT move the index that shows us what move we
     * are at.
     */
    public Move getUndo() {
        LastDirection lastDirection = moveHistory.getLastDirection();
        if (lastDirection == null)
            return null;

        Move move = null;

        switch (lastDirection) {
            case FORWARD:
                moveHistory.setLastDirection(BACK);
                move = moveHistory.getCurrMove();
                break;

            case BACK:
                if (!moveHistory.isIndexFirst())
                    moveHistory.decremIndex();
                if (!moveHistory.isIndexFirst())
                    move = moveHistory.getCurrMove();
                break;
        }
        return move;
    }

    /**
     *
     * @return returns object Move to use in other methods
     * The move  we need is obtained depending on which direction we went last time and whether there are moves left
     * to go in the direction we want. lastDirection shows us whether we went forward or backward in move history. If we
     * went forward and want to go forward, it works normally. Backward + backward = normally. Backward and then forward
     * or the other way around - we only switch values from last move and NOT move the index that shows us what move we
     * are at.
     */
    public Move getRedo() {
        LastDirection lastDirection = moveHistory.getLastDirection();
        if (lastDirection == null)
            return null;

        Move move = null;

        switch (lastDirection) {

            case FORWARD:
                if (!moveHistory.isIndexLast())
                    moveHistory.incremIndex();
                move = moveHistory.getCurrMove();
                break;

            case BACK:
                if (moveHistory.isIndexFirst())
                    moveHistory.incremIndex();
                moveHistory.setLastDirection(FORWARD);
                move = moveHistory.getCurrMove();
        }
        return move;
    }

    public void clearHistory() {
        moveHistory.clear();
        moveHistory.setIndex(-1);
    }
}
