package main.controller;


import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import main.model.FileOperations;
import main.model.ModelBoard;
import main.model.ModelBoardDefaults;
import main.model.history.Move;
import main.utils.Utils;

import java.lang.reflect.GenericArrayType;
import java.util.ArrayList;

import static main.controller.ValueType.NEW_VALUE;
import static main.controller.ValueType.OLD_VALUE;

/**
 * <p>rfegrwtbhtbh
 * <p>werb {@link ModelBoard} link  sdfsf dfgsg fgfsg <code>getDefaultValues</code>
 */

/**
 * Operates the board, processing queries and sending them to both GameView and ModelBoard, as well as other parts of
 * view and model packages
 */
public class ControlBoard {

    private ModelBoard modelBoard;
    private ModelBoardDefaults modelBoardDefaults;
    private ControlHistory controlHistory;
    private FileOperations fileOperations;

    private int width, height;

    //------------------------------------------------------------------------------
    public ControlBoard(
            ModelBoard modelBoard,
            ModelBoardDefaults modelBoardDefaults,
            ControlHistory controlHistory,
            int width, int height,
            FileOperations fileOperations) {

        this.modelBoard = modelBoard;
        this.modelBoardDefaults = modelBoardDefaults;
        this.controlHistory = controlHistory;
        this.fileOperations = fileOperations;
        this.width = width;
        this.height = height;
    }

    //------------------------------------------------------------------------------
    public int[][] getCurrGameState() {
        return modelBoard.getBoardValues();
    }

    //------------------------------------------------------------------------------
    public int[][] getDefaultValues() {
        int[][] valuesArr = modelBoardDefaults.getDefaultBoard(width, height); //resetToDefault;
        modelBoard.setBoardAllFields(valuesArr, width, height);
        return valuesArr;
    }

    //------------------------------------------------------------------------------

    /**
     *
     * @param toCheck Array of values from game field that we check for being correct
     * @param width Width of board
     * @param height Height of board
     * @return ArrayList of fields that are incorrect according to game logic
     */
    public ArrayList<int[]> checkCorrect(int[][] toCheck, int width, int height) {

        ArrayList<int[]> badFields = new ArrayList<int[]>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                boolean isSquare = Utils.getInstance().isSquare(x, y);
                if (isSquare && equalsNeighTotal(toCheck, x, y, width, height) || (isSquare && toCheck[x][y] == 0)) {
                    badFields.add(new int[]{x, y});
                }
            }
        }
        return badFields;
    }

    //------------------------------------------------------------------------------

    /**
     * Checks game logic
     *
     * @param arrToCheck Array from we take all 5 values required
     * @param i X of the element we are checking
     * @param j Y of the element we are checking
     * @param width Width of the array we're checking
     * @param height Height of the array we're checking
     * @return Whether the square is equal the sum of neighbouring circles
     */
    private boolean equalsNeighTotal(int[][] arrToCheck, int i, int j, int width, int height) {
        return !(arrToCheck[i][j] ==
                getNeighbour(arrToCheck, i, j - 1, width, height)
                        + getNeighbour(arrToCheck, i - 1, j, width, height)
                        + getNeighbour(arrToCheck, i + 1, j, width, height)
                        + getNeighbour(arrToCheck, i, j + 1, width, height));
    }

    //------------------------------------------------------------------------------

    /**
     * Returns value in a field, returning 0 if it's out of bounds
     *
     * @param arrToCheck
     * @param x X of element we're retrieving
     * @param y Y of element we're retrieving
     * @param width Width of the array of values
     * @param height Height of the array of values
     * @return Value in the field we're retrieving
     */
    private int getNeighbour(int[][] arrToCheck, int x, int y, int width, int height) {
        if (x >= 0 && x < width) {
            if (y >= 0 && y < height) {
                return arrToCheck[x][y];
            }
        }
        return 0;
    }

    // MOVES HISTORY
    //------------------------------------------------------------------------------

    /**
     * Saves into an ArrayList of moves new Move, containing old value, new value, x&y of changed field
     *
     * @param x X of field we changed value in
     * @param y Y of field we changed value in
     * @param newValue New value we are saving
     */
    public void saveMove(int x, int y, int newValue) {
        int oldValue = modelBoard.getFieldValue(x, y);
        if (oldValue == newValue)
            return;
        Move move = new Move(x, y, oldValue, newValue);
        controlHistory.save(move);
        modelBoard.setFieldValue(move, NEW_VALUE);
    }

    //------------------------------------------------------------------------------

    /**
     * Connects undo operations in model and view
     *
     * @return Move for GameView to use in undoing the move
     */
    public Move undo() {
        Move move = controlHistory.getUndo();
        if (move != null)
            modelBoard.setFieldValue(move, OLD_VALUE);
        return move;
    }

    //------------------------------------------------------------------------------
    /**
     * Connects redo operations in model and view
     *
     * @return Move for GameView to use in uredoing the move
     */
    public Move redo() {
        Move move = controlHistory.getRedo();
        if (move != null)
            modelBoard.setFieldValue(move, NEW_VALUE);
        return move;
    }

    //------------------------------------------------------------------------------

    /**
     * Empties move history
     */
    public void resetHistory() {
        controlHistory.clearHistory();
    }


    // GAME RESULT
    //------------------------------------------------------------------------------

    /**
     * Returns ArrayList, with each piece being an array of pairs of coordinates of incorrect fields; if all are correct
     * returns an empty array.
     *
     * @param gameGrid All values in the game, to be checked for being correct
     * @return ArrayList of incorrect field coordinates
     */
    public ArrayList<int[]> isVictorious(GridPane gameGrid) {
        //TODO Change both 6 to a variable
        int width = 6;
        int height = 6;
        ArrayList<int[]> incorrectFields = checkCorrect(modelBoard.getBoardValues(), width, height);


        if (incorrectFields.isEmpty()) {
            System.out.println("All squares are properly filled!");
            dialogYouWin();
        } else {
            System.out.println("Some squares are still wrong or not filled at all.");
        }
        return incorrectFields;
    }

    //------------------------------------------------------------------------------

    /**
     * Shows a dialog "YOU WIN" when called
     */
    public void dialogYouWin() {
        Dialog victoryPrompt = new Dialog();
        victoryPrompt.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = victoryPrompt.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(((Node) closeButton).visibleProperty());
//        closeButton.setVisible(false);
        victoryPrompt.setHeaderText("");
        victoryPrompt.setContentText("YOU WIN");
        victoryPrompt.showAndWait();
    }

    //------------------------------------------------------------------------------
    public void saveGame() {
        fileOperations.saveStateToFile(modelBoard.getBoardValues(), height, width, "output");
    }

    //------------------------------------------------------------------------------
    public int[][] loadSave() {
        int[][] valuesArr = fileOperations.readFromFile(modelBoard.getBoardValues(), "output");
        modelBoard.setBoardAllFields(valuesArr, width, height);
        return valuesArr;
    }
}
