package main.model;

import main.controller.ValueType;
import main.model.history.Move;

/**
 * Holds all data about our current game board, such as values, and provides getters & setters for them, including
 * methods allowing to obtain default board and it's values.
 */
public class ModelBoard {

    int howFarBackSave = 100;
    int width = 6;
    int height = 6;

    /**
     * Holds values from boardfield so they're easily accessible
     */
    private int[][] boardValues = new int[width][height];
    //    private int[] movesArray = new int[howFarBackSave];


    // =========================================================================================================
    // for Mockito Spy
    public ModelBoard(){}

    public ModelBoard(int width, int height) {
        this.width = width;
        this.height = height;
    }

    // =========================================================================================================

    /**
     * Sets value of both board field that is displayed and one in the model data storage.
     *
     * @param move Move provides all the data we need to find where and what we want to save
     * @param valueType Tells us whether it's an old value or if it's new
     */
    public void setFieldValue(Move move, ValueType valueType) {
        int value = valueType == ValueType.NEW_VALUE ? move.newValue : move.oldValue;
        boardValues[move.x][move.y] = value;
    }
    //---------------------------------------------------------------------------------
    public int getFieldValue(int x, int y) {
        return boardValues[x][y];
    }

    //---------------------------------------------------------------------------------

    /**
     * Changes displayed values to ones in provided array called values
     *
     * @param values values we want to display
     * @param width self eplanatory
     * @param height self eplanatory
     */
    public void setBoardAllFields(int[][] values, int width, int height) {
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                boardValues[x][y] = values[x][y];
//        values[x][y].setText("" + boardValues[x][y]);
    }

    //---------------------------------------------------------------------------------
    public int[][] getBoardValues() {
        return boardValues;
    }

    //------------------------------------------------------------------------
    private void fillBoardFromArr(int width, int height, int[][] provided) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                boardValues[x][y] = provided[x][y];
            }
        }
    }
}