package main.model;

import main.utils.IntStringChanger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FileOperations {


    //---------------------------------------------------------------------------------

    /**
     * Saves current game to file in a following format:
     *
     * width
     * height
     * [Array]
     *
     * @param numbersFromBoard All values from board, as they currently are
     * @param height Height of the board
     * @param width Width of the board
     * @param saveName Name of the save file
     */
    public void saveStateToFile(int[][] numbersFromBoard, int height, int width, String saveName) {
        //ArrayList<int[]> movesList,
        String fileName = saveName;

        try (FileOutputStream outputStream = new FileOutputStream(fileName);
             OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
             Writer writer = new BufferedWriter(streamWriter)) {

            StringBuilder stringBuilder = new StringBuilder();

            String saveableHeight = height + "\n";
            String saveableWidth = width + "\n";

            stringBuilder.append(saveableHeight);
            stringBuilder.append(saveableWidth);

            stringBuilder.append(IntStringChanger.intToString(numbersFromBoard, height, width));
            writer.write(String.valueOf(stringBuilder));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //---------------------------------------------------------------------------------

    /**
     * Reads array and it's sizes from file, then assigns proper values to variables in code
     *
     * @param boardValues Array we're filling from file
     * @param saveName Save file we're reading from
     * @return Array obtained array
     */
    public int[][] readFromFile(int[][] boardValues, String saveName) {

        int height = boardValues.length;
        int width = boardValues[0].length;
        int[][] arrFromFile = IntStringChanger.stringToArrInt(saveName);
        int fromFileHeight = arrFromFile.length;
        int fromFileWidth = arrFromFile[0].length;

        if (fromFileHeight == height && fromFileWidth == width) {
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                    boardValues[x][y] = arrFromFile[x][y];
        } else {
            System.out.println("Boardfield dimensions don't match ones in the save file");
        }
        return arrFromFile;
    }
}