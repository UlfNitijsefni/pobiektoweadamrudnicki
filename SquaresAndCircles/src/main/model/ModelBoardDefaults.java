package main.model;

public class ModelBoardDefaults {

    //---------------------------------------------------------------------------------

    /**
     * Hard coded default board
     *
     * @param width width of board, leftover from trying to create adjustable board
     * @param height height of board, leftover from trying to create adjustable board
     * @return Retruns default board values
     */
    public int[][] getDefaultBoard(int width, int height) {
        int[][] numbersFromBoard = new int[width][height];
        numbersFromBoard[0][0] = 6;
        numbersFromBoard[0][2] = 10;
        numbersFromBoard[0][4] = 9;
        numbersFromBoard[0][5] = 4;
        numbersFromBoard[1][3] = 7;
        numbersFromBoard[2][4] = 5;
        numbersFromBoard[3][1] = 7;
        numbersFromBoard[3][3] = 10;
        numbersFromBoard[3][5] = 4;
        numbersFromBoard[4][2] = 10;
        numbersFromBoard[4][4] = 8;
        numbersFromBoard[5][1] = 5;
        numbersFromBoard[5][3] = 6;
        return numbersFromBoard;
    }
}
