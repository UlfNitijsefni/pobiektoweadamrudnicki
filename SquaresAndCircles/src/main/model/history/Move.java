package main.model.history;

public final class Move {
    public int x;
    public int y;
    public int oldValue;
    public int newValue;

    public Move(int x, int y, int oldValue, int newValue) {
        this.x = x;
        this.y = y;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }
}
