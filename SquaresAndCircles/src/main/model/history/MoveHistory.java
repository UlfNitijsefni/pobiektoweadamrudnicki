package main.model.history;

import java.util.ArrayList;

/**
 * Used to operate, access, and edit history of moves done by the user
 */
public class MoveHistory {

    int howFarBackSave = 100;
    int currUndoIndex = -1;
    private ArrayList<Move> movesList = new ArrayList<Move>();
    private LastDirection lastDirection;

    public boolean isEmpty() {
        return movesList.isEmpty();
    }

    public boolean isIndexLast() {
        return currUndoIndex == movesList.size() - 1;
    }

    public boolean isIndexFirst() {
        return currUndoIndex == -1;
    }

    public int getSize() {
        return movesList.size();
    }

    public int getIndex() {
        return currUndoIndex;
    }

    public void setIndex(int index) {
        this.currUndoIndex = index;
    }

    public void incremIndex() {
        currUndoIndex++;
    }

    public void decremIndex() {
        currUndoIndex--;
    }

    public void dropTail() {
        movesList.subList(currUndoIndex + 1, movesList.size()).clear();
    }

    public void addMove(Move move) {
        movesList.add(move);
    }

    public Move getCurrMove() {
        return movesList.get(currUndoIndex);
    }

    /**
     * Deletes history of all moves done to date, leaving us with an empty ArrayList
     */
    public void clear() {
        movesList = new ArrayList<Move>();
    }

    public MoveHistory(int howFarBack) {
        this.howFarBackSave = howFarBack;
    }

    /**
     * Allows to check whether last move was undoing or rather redoing a move
     *
     * @return Returns ENUM simply telling us whether last move was forward or backward. Could be boolean with only two
     * options, but this is more easily understood
     */
    public LastDirection getLastDirection() {
        return lastDirection;
    }

    public void setLastDirection(LastDirection lastDirection) {
        this.lastDirection = lastDirection;
    }

    // for Mockito Spy
    public MoveHistory() {
    }
}