package application;
	
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class DrawingLine extends Application {
   @Override     
   public void start(Stage primaryStage) throws Exception {
		Line lineTest = new Line();
		lineTest.setStartX(100.0); 
		lineTest.setStartY(150.0); 
		lineTest.setEndX(300.0); 
		lineTest.setEndY(250.0);
		Group root = new Group(lineTest);
   }
}
 