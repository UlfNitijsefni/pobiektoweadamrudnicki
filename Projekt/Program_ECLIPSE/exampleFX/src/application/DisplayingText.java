package application;

import javafx.application.Application; 
import javafx.collections.ObservableList; 
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.stage.Stage; 
import javafx.scene.text.Font; 
import javafx.scene.text.Text; 
         
public class DisplayingText extends Application { 
   @Override 
   public void start(Stage stage) {
      Text text = new Text(); 
      text.setFont(new Font(50));
      
      //setting the position of the text
      text.setX(50);
      text.setY(150);
      
      //Setting the text to be added.
      text.setText("Test text");
      Group root = new Group();
      ObservableList list = root.getChildren();
      
      //Setting the text object as a node to the group object
      list.add(text);        
      Scene scene = new Scene(root, 600, 300);  
      stage.setTitle("Sample Application"); 
      stage.setScene(scene);  
      stage.show();
   }   
   public static void main(String args[]){ 
      launch(args);
   } 
} 