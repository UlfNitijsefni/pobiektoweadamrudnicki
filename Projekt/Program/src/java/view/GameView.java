package java.view;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.paint.Color;


public class GameView extends Application {
public int heightOfBoard = 900;
public int widthOfBoard = 900;
public int paddingSize = 10;
    @Override
    public void start(Stage stage) {

//        static DropShadow shadowhoover = new DropShadow(5, 4, 4, Color.rgb(50, 60, 50));
//        static DropShadow shadowdown = new DropShadow(2, 2, 2, Color.rgb(50, 60, 50));
        //========================================================================

        Text testText = new Text("Testing Playing Field");
        Text testText2 = new Text("Testing Buttons");
//        mainHBox.getChildren().add(testText);

        GridPane gameSetGrid = new GridPane();
        gameSetGrid.setGridLinesVisible(true);


        gameSetGrid.setMinSize(widthOfBoard, heightOfBoard);

        gameSetGrid.setPadding(new Insets(paddingSize, paddingSize, paddingSize, paddingSize));


        //Fill game grid with textfields, with the text inputed into them aligned in the middle of the field
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                TextField inForText = new TextField();

                inForText.setPrefSize((heightOfBoard - 2*paddingSize) / 6, (heightOfBoard - 2*paddingSize) / 6);
                inForText.setAlignment(Pos.CENTER);

                gameSetGrid.add(inForText, i, j);
            }
        }
        //------------------------------------------------------------------------
//        Button startButton = new Button("Start");
//        Button checkButton = new Button("Check");

//        checkButton.setPrefSize(widthOfBoard / 10, widthOfBoard / 10);
//        startButton.setPrefSize(widthOfBoard / 10, widthOfBoard / 10);
//
//        GridPane buttonsGrid = new GridPane();
//        buttonsGrid.setGridLinesVisible(true);
//        buttonsGrid.add(testText2, 1, 1);
//
//        buttonsGrid.setMinSize(widthOfBoard / 9, heightOfBoard);
//
//        buttonsGrid.setPadding(new Insets(10, 10, 10, 10));
//
//        buttonsGrid.add(startButton, 1, 10);
////        startButton.alignmentProperty(CENTER);
//        buttonsGrid.add(checkButton, 1, 15);

        //------------------------------------------------------------------------

        Button startButton = new Button("Start");
        Button checkButton = new Button("Check");
        checkButton.setAlignment(Pos.CENTER);
        startButton.setAlignment(Pos.CENTER);

        VBox buttonBox = new VBox(startButton, checkButton);

        //------------------------------------------------------------------------

        Color red = Color.rgb(0,0,255);
//        gameSetGrid.setBorder(BorderFactory.createLineBorder(javafx.scene.layout.Border, 2));
        gameSetGrid.setStyle(" -fx-border-color: blue;  -fx-border-width: 2 ; ");
//        buttonsGrid.setStyle(" -fx-border-color: green;  -fx-border-width: 2 ; ");

//        HBox mainHBox = new HBox(gameSetGrid, buttonsGrid);
        HBox mainHBox = new HBox(gameSetGrid, buttonBox);
        buttonBox.setPrefSize(widthOfBoard / 9, heightOfBoard);
        buttonBox.setMinSize(widthOfBoard / 9, heightOfBoard);

        //------------------------------------------------------------------------
        //Creating a scene object
//      Scene scene = new Scene(gameSetGrid);
        Scene scene1 = new Scene(mainHBox);
        stage.setTitle("Hbox Example");
        stage.setScene(scene1);
        stage.show();
    }
    public static void main(String args[]){
        launch(args);
    }
}